<?php
//database config
define('DB_HOST', '127.0.0.1');
define('DB_NAME', 'csc8019_team11');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');

//the ice cream money of diffierent kind of size
define('SMALL_MONEY', 1.75);
define('MEDIUM_MONEY', 2.25);
define('LARGE_MONEY', 2.75 );
define('EXTRA_LARGE_MONEY', 3.50);
define('EXTRA_EXTRA_LARGE_MONEY', 5.75);