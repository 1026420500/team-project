<?php session_start();?>
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>Welcome</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="./css/font.css">
        <link rel="stylesheet" href="./css/xadmin.css">
        <script src="./lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="./js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <blockquote class="layui-elem-quote">Welcome ：
                                <span class="x-red"><?=$_SESSION['user']['username']?></span>！Current time:<?=date('Y-m-d H:i:s');?>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="layui-col-md12">
                    <div class="layui-card" style="text-align: center;height: 300px;">
                        <div class="layui-card-body">
                            <p style="font-size: 40px;color: #ccc;line-height: 300px;">Order management system</p>
                        </div>
                    </div>
                </div>

                <style id="welcome_style"></style>
            </div>
            <div class="layui-col-md12" style="height:50px; line-height: 50px; z-index:9999;position:fixed;bottom: 0px;text-align: center;background: #eee;color: #666;">
                Adress: Avenue Cresent，Seaton Delaval，Northumberland NE25 0DN
            </div>
        </div>

    </body>
</html>